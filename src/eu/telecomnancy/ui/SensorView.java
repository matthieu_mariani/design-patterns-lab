package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.AcquireCommande;
import eu.telecomnancy.sensor.IProxySensor;
import eu.telecomnancy.sensor.OffCommande;
import eu.telecomnancy.sensor.OnCommande;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SensorView  extends JPanel implements ISensorObserver {
    private IProxySensor sensor;

    private JLabel value;
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    
    private OnCommande OnCmd;
    private OffCommande OffCmd;
    private AcquireCommande UpdateCmd;
    

    public void update(){ 
    		
    	double value;
    	try {
    		value = sensor.getValue();
    		this.value.setText(value + "°C");
    	}
    	catch(SensorNotActivatedException e) {
    		this.value.setText("N/A °C");
    	}
    		
            
        
        };
        
    public SensorView(IProxySensor c) {
    
        this.sensor = c;
        OnCmd = new OnCommande(sensor);
        OffCmd = new OffCommande(sensor);
        UpdateCmd = new AcquireCommande(sensor);
        

        c.addObs(this);
        this. value = new JLabel("N/A °C");
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OnCmd.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               OffCmd.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    UpdateCmd.execute();
                } catch (SensorNotActivatedException sensorNotActivatedException) {
                    sensorNotActivatedException.printStackTrace();
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }
	
}
