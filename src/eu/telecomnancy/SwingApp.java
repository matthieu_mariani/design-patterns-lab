package eu.telecomnancy;


import eu.telecomnancy.decorator.FahrenheitConvertor;
import eu.telecomnancy.decorator.RoundConvertor;
import eu.telecomnancy.sensor.IProxySensor;
import eu.telecomnancy.sensor.ProxySensor;

import eu.telecomnancy.sensor.SimpleSensorLogger;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        IProxySensor sensor = new RoundConvertor(new ProxySensor(new TemperatureSensor(),new SimpleSensorLogger ()));
        new MainWindow(sensor);
    }

}
