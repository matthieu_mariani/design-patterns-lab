package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Random;

import eu.telecomnancy.ui.ISensorObserver;

public class TemperatureSensor implements ISensor {
    boolean state;
    double value = 0;
    private ArrayList<ISensorObserver> tabObservateur;
    
    public TemperatureSensor(){
    	state = false;
    	tabObservateur  = new ArrayList<ISensorObserver>();
    	
    }

    public void addObs(ISensorObserver obs ){
    	tabObservateur.add(obs); 	
    }
    public void removeObs(ISensorObserver obs){
    	tabObservateur.remove(obs);
    }
    
    public void notifyObs()
    {
    
            for(int i=0;i<tabObservateur.size();i++)
            {
            	ISensorObserver obs =  (ISensorObserver) tabObservateur.get(i);
            	obs.update();
            	}
                    
                    
           
    }

    @Override
    public void on() {
   
        state = true;
        notifyObs();
    }

    @Override
    public void off() {
        state = false;
        notifyObs();
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state){
            value = (new Random()).nextDouble() * 100;
            System.out.println("Valuer : " + value);
        	notifyObs();
        }else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
