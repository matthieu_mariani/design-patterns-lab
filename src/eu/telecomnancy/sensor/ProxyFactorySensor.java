package eu.telecomnancy.sensor;

public class ProxyFactorySensor {
	  public IProxySensor getSensor() {
	        return new ProxySensor(new TemperatureSensor(), new SimpleSensorLogger());
	    }

}
