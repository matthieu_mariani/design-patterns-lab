package eu.telecomnancy.sensor;

public class AcquireCommande implements Commande {
    
	IProxySensor sensor;
	
	public AcquireCommande (IProxySensor s){
		this.sensor = s;
	}

	public void execute() throws SensorNotActivatedException {
		sensor.update();
	}


}
