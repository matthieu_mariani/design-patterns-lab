package eu.telecomnancy.sensor;

public interface Sensorstate {
	public void on();
	public void off();
	public void update() throws SensorNotActivatedException;
	public boolean getStatus();
	public double getValue() throws SensorNotActivatedException;

}
