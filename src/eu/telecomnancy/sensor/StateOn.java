package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOn implements Sensorstate {
	
	private ContextSensor sensor;
	//constructeur
	public StateOn(ContextSensor sensor){
		this.sensor = sensor;
	}
	
	
	//méthodes
	public void on() {
	}
	public void off() {
		sensor.setSensorStateChange(sensor.getStateOff());
		sensor.notifyObs();
	}
	public void update()  {
		sensor.value = (new Random()).nextDouble() * 100;
    	sensor.notifyObs();
	}
	public boolean getStatus() {
		return sensor.state;
	}
	public double getValue()  {
		return sensor.value;
	}

}
