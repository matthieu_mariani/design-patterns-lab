package eu.telecomnancy.sensor;

public class OffCommande implements Commande {
    
	IProxySensor sensor;
	
	public  OffCommande(IProxySensor s){
		this.sensor = s;
	}
	
	public void execute() {
		sensor.off();
	}
}
