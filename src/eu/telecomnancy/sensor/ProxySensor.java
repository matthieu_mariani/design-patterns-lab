package eu.telecomnancy.sensor;

import java.util.Date;

import eu.telecomnancy.ui.ISensorObserver;

public class ProxySensor implements IProxySensor {
	
	protected ISensor sensor;
	protected SensorLogger log;
	
	


	
	public ProxySensor (ISensor sensor , SensorLogger logger){
		this.sensor = sensor;
		this.log = logger;
	}
	
	
	
	public void addObs(ISensorObserver obs) {
		sensor.addObs(obs);	
	}


		

	public void on() {
		log.log(LogLevel.INFO, "méthode on() appelée");
		log.log(LogLevel.INFO, (new Date()).toString());
		sensor.on();
	}
	
	public void off() {
		log.log(LogLevel.INFO," méthode off() appelée" );
		log.log(LogLevel.INFO, (new Date()).toString());
		sensor.off();
	}
	
	public boolean getStatus() {
		log.log(LogLevel.INFO, "méthode getStatus appelée");
		log.log(LogLevel.INFO, (new Date()).toString());
		return sensor.getStatus();	
	}
	
	public void update() throws SensorNotActivatedException {
		log.log(LogLevel.INFO, "méthode update() appelée");
		log.log(LogLevel.INFO, (new Date()).toString());
		sensor.update();
	}
	
	public double getValue() throws SensorNotActivatedException {
		log.log(LogLevel.INFO, "valeur du capteur = " + sensor.getValue());
		log.log(LogLevel.INFO, (new Date()).toString());
		return sensor.getValue();
		
	}

}
