package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.ISensorObserver;

public interface IProxySensor {

	public void addObs(ISensorObserver obs);
	public void on();
	public void off();
	public boolean getStatus();
	public void update() throws SensorNotActivatedException;
	public double getValue() throws SensorNotActivatedException;
	
}
