package eu.telecomnancy.sensor;

public class OnCommande implements Commande {
	
	
	IProxySensor sensor;
	
	public OnCommande(IProxySensor s){
		this.sensor = s;
	}

	public void execute()  {
		sensor.on();
	}

}
