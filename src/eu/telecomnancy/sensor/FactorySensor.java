package eu.telecomnancy.sensor;

import java.io.IOException;
import java.util.Properties;

import eu.telecomnancy.helpers.ReadPropertyFile;

public abstract class FactorySensor {
	 public static ISensor makeSensor() {
	        ReadPropertyFile rp = new ReadPropertyFile();
	        Properties p = null;
	        try {
	            p = rp.readFile("/eu/telecomnancy/app.properties");
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        String factory = p.getProperty("factory");
	        ISensor sensor;
	        sensor = null;
	        try {
	            FactorySensor sensorfactory = (FactorySensor) Class.forName(factory).newInstance();
	            sensor = sensorfactory.getSensor();

	        } catch (InstantiationException e) {
	            e.printStackTrace();
	        } catch (IllegalAccessException e) {
	            e.printStackTrace();
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	        return sensor;
	    }

	    public abstract ISensor getSensor();

}
