package eu.telecomnancy.sensor;

public class SimpleFactorySensor extends FactorySensor {
    public ISensor getSensor() {
        return new TemperatureSensor();
    }

}
