package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.ui.ISensorObserver;

public class ContextSensor implements ISensor {
	
	boolean state;
    double value = 0;
    
	
	Sensorstate On = new StateOn(this);
	Sensorstate Off = new StateOff(this);
	Sensorstate currentstate = Off;
	
	private ArrayList<ISensorObserver> tabObservateur = new ArrayList<ISensorObserver>();
	
	//méthodes de changement d'états
	public Sensorstate getStateOn(){
		return this.On;
	}
	
	public Sensorstate getStateOff(){
		return this.Off;
	}
	
	public void setSensorStateChange(Sensorstate state){
		this.currentstate = state;
	}
	
	
	
	//méthode observer
    public void addObs(ISensorObserver obs ){
    	tabObservateur.add(obs); 	
    }
    public void removeObs(ISensorObserver obs){
    	tabObservateur.remove(obs);
    }
    public void notifyObs()
    {
    
            for(int i=0;i<tabObservateur.size();i++)
            {
            	ISensorObserver obs =  (ISensorObserver) tabObservateur.get(i);
            	obs.update();
            	}    
    }
    
    
    
	public void on() {
		 currentstate.on();
	}
	public void off() {
		currentstate.off();
	}
	public boolean getStatus() {
		return currentstate.getStatus();
	}
	
	
	
	public void update() throws SensorNotActivatedException {
		currentstate.update();
		}
	public double getValue() throws SensorNotActivatedException{
		return currentstate.getValue();
	}

}
