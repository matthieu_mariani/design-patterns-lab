package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.ISensorObserver;

public interface ISensorObservable {
	public void addObs(ISensorObserver obs);
	public void removeObs(ISensorObserver obs);
	public void notifyObs();
}
