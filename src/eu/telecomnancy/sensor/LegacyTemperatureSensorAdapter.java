/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.ui.ISensorObserver;

/**
 *
 * @author matthieumariani
 */
public class LegacyTemperatureSensorAdapter implements ISensor{
    
    private LegacyTemperatureSensor LTS ;
    private ArrayList tabObservateur; 
   
    public LegacyTemperatureSensorAdapter (){
        super();
        LTS = new LegacyTemperatureSensor();
        }
    
    public void addObs(ISensorObserver obs ){
    	tabObservateur.add(obs); 	
    }
    public void removeObs(ISensorObserver obs){
    	tabObservateur.remove(obs);
    }
    
    public void notifyObs()
    {
    
            for(int i=0;i<tabObservateur.size();i++)
            {
            	ISensorObserver obs =  (ISensorObserver) tabObservateur.get(i);
            	obs.update();
            	}
                    
                    
           
    }

    
    
    public void on(){
        if(!this.LTS.getStatus()){ //si le sensor est éteint
            this.LTS.onOff(); // on l'allume
        }
    };

    public void off(){
        if(this.LTS.getStatus()){ // si le sensor est allumé
             this.LTS.onOff(); // on l'éteint
        }
    }

    public boolean getStatus(){
       return this.LTS.getStatus();
    }

    public void update() throws SensorNotActivatedException{
        this.LTS.onOff();
        this.LTS.onOff();
    };

    public double getValue() throws SensorNotActivatedException{
    	if(this.LTS.getStatus()) return LTS.getTemperature();
    	throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    	
    };
    
}
