package eu.telecomnancy.sensor;

public class StateOff implements Sensorstate{
	private ContextSensor sensor;
	
	//constructeur
	public StateOff(ContextSensor sensor){
		this.sensor =sensor;
	}
	
	
	
	//méthodes
	public void on() {
		sensor.setSensorStateChange(sensor.getStateOn());
		sensor.notifyObs();
	}
	public void off() {
	}
	public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}
	public boolean getStatus() {
		return sensor.state;
	}
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

}
