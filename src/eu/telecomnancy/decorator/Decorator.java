package eu.telecomnancy.decorator;



import eu.telecomnancy.sensor.IProxySensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.ui.ISensorObserver;


public abstract class Decorator implements  IProxySensor {		
	protected IProxySensor sensor;
	
	public Decorator( IProxySensor sensor){
		this.sensor= sensor;	
	}


	public void on() {
		sensor.on();
	}
	
	public void off() {
		sensor.off();
	}
	
	public boolean getStatus() {
		return sensor.getStatus();	
	}
	
	public void update() throws SensorNotActivatedException {
		sensor.update();		
	}
	
	public void addObs(ISensorObserver obs) {
		sensor.addObs(obs);
		
	}
	
	public double getValue() throws SensorNotActivatedException {
		return sensor.getValue();
	}
}
