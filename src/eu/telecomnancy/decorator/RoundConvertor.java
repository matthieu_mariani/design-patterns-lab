package eu.telecomnancy.decorator;

import eu.telecomnancy.sensor.IProxySensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import java.lang.Math;

public class RoundConvertor extends Decorator {
	
	public RoundConvertor(IProxySensor sensor){
		super(sensor);
	}
	
	public double getValue() throws SensorNotActivatedException{
		double res = this.sensor.getValue();
		res = Math.round(res);
		System.out.println("Valeur arrondie : " + res);
		return res;
	}

}
