package eu.telecomnancy.decorator;

import eu.telecomnancy.sensor.IProxySensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;


public class FahrenheitConvertor extends Decorator {
	public FahrenheitConvertor(IProxySensor sensor) {
		super(sensor);
	}
	

	public double getValue() throws SensorNotActivatedException {
		double tempValue = sensor.getValue() * 1.8 + 32;
		System.out.println("la valeur en fahrenheit est " + tempValue);
		return tempValue;
	}


}
